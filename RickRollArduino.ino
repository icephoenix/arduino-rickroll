/**
 * MIDI-Arduino Sequencing code generator
 * Courtesy of http://greenlightgo.org/projects/midi-to-arduino/
 **/

#define msPerTick 1

void setup(){
  pinMode(8, OUTPUT);

  Serial.begin(9600);
  Serial.println("Programm your own sounds!");
  Serial.println("Tutorial: https://www.youtube.com/watch?v=8L78MDwr3C0");
  Serial.println("Source Code: ");
}

void loop(){
  doPerformance();
}

void doPerformance(){

  delay(484 * msPerTick);

  tone(8, 233);

  delay(215 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 262);

  delay(230 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 277);

  delay(175 * msPerTick);

  noTone(8);

  delay(76 * msPerTick);

  tone(8, 277);

  delay(240 * msPerTick);

  noTone(8);

  delay(4 * msPerTick);

  tone(8, 311);

  delay(250 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 262);

  delay(325 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 233);

  delay(156 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 208);

  delay(1199 * msPerTick);

  noTone(8);

  delay(681 * msPerTick);

  tone(8, 233);

  delay(180 * msPerTick);

  noTone(8);

  delay(64 * msPerTick);

  tone(8, 233);

  delay(235 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 262);

  delay(246 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 277);

  delay(235 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 233);

  delay(340 * msPerTick);

  noTone(8);

  delay(140 * msPerTick);

  tone(8, 208);

  delay(230 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 415);

  delay(380 * msPerTick);

  noTone(8);

  delay(109 * msPerTick);

  tone(8, 415);

  delay(236 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 311);

  delay(1190 * msPerTick);

  noTone(8);

  delay(244 * msPerTick);

  tone(8, 233);

  delay(165 * msPerTick);

  noTone(8);

  delay(86 * msPerTick);

  tone(8, 233);

  delay(235 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 262);

  delay(239 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 277);

  delay(250 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 233);

  delay(231 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 277);

  delay(235 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 311);

  delay(244 * msPerTick);

  noTone(8);

  delay(235 * msPerTick);

  tone(8, 262);

  delay(231 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 233);

  delay(245 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 262);

  delay(105 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 233);

  delay(119 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 208);

  delay(785 * msPerTick);

  noTone(8);

  delay(390 * msPerTick);

  tone(8, 233);

  delay(165 * msPerTick);

  noTone(8);

  delay(81 * msPerTick);

  tone(8, 233);

  delay(230 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 262);

  delay(239 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 277);

  delay(150 * msPerTick);

  noTone(8);

  delay(91 * msPerTick);

  tone(8, 233);

  delay(245 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 208);

  delay(264 * msPerTick);

  noTone(8);

  delay(190 * msPerTick);

  tone(8, 311);

  delay(185 * msPerTick);

  noTone(8);

  delay(71 * msPerTick);

  tone(8, 311);

  delay(134 * msPerTick);

  noTone(8);

  delay(96 * msPerTick);

  tone(8, 311);

  delay(244 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 349);

  delay(245 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 311);

  delay(495 * msPerTick);

  noTone(8);

  delay(471 * msPerTick);

  tone(8, 277);

  delay(1180 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 311);

  delay(235 * msPerTick);

  noTone(8);

  delay(4 * msPerTick);

  tone(8, 349);

  delay(230 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 277);

  delay(220 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 311);

  delay(201 * msPerTick);

  noTone(8);

  delay(60 * msPerTick);

  tone(8, 311);

  delay(145 * msPerTick);

  noTone(8);

  delay(70 * msPerTick);

  tone(8, 311);

  delay(249 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 349);

  delay(226 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 311);

  delay(220 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 208);

  delay(220 * msPerTick);

  noTone(8);

  delay(59 * msPerTick);

  tone(8, 208);

  delay(726 * msPerTick);

  noTone(8);

  delay(705 * msPerTick);

  tone(8, 233);

  delay(235 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 262);

  delay(234 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 277);

  delay(230 * msPerTick);

  noTone(8);

  delay(6 * msPerTick);

  tone(8, 233);

  delay(230 * msPerTick);

  noTone(8);

  delay(254 * msPerTick);

  tone(8, 311);

  delay(250 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 349);

  delay(221 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 311);

  delay(469 * msPerTick);

  noTone(8);

  delay(255 * msPerTick);

  tone(8, 208);

  delay(116 * msPerTick);

  noTone(8);

  delay(4 * msPerTick);

  tone(8, 233);

  delay(101 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 277);

  delay(94 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 233);

  delay(116 * msPerTick);

  noTone(8);

  delay(20 * msPerTick);

  tone(8, 349);

  delay(259 * msPerTick);

  noTone(8);

  delay(146 * msPerTick);

  tone(8, 349);

  delay(330 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 311);

  delay(494 * msPerTick);

  noTone(8);

  delay(226 * msPerTick);

  tone(8, 208);

  delay(85 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 233);

  delay(110 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 262);

  delay(114 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 233);

  delay(121 * msPerTick);

  noTone(8);

  delay(25 * msPerTick);

  tone(8, 311);

  delay(315 * msPerTick);

  noTone(8);

  delay(94 * msPerTick);

  tone(8, 311);

  delay(331 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 277);

  delay(279 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 262);

  delay(115 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 233);

  delay(265 * msPerTick);

  noTone(8);

  delay(56 * msPerTick);

  tone(8, 208);

  delay(109 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 233);

  delay(130 * msPerTick);

  noTone(8);

  delay(10 * msPerTick);

  tone(8, 262);

  delay(96 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 233);

  delay(104 * msPerTick);

  noTone(8);

  delay(15 * msPerTick);

  tone(8, 277);

  delay(436 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 311);

  delay(250 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 262);

  delay(224 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 233);

  delay(235 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 208);

  delay(276 * msPerTick);

  noTone(8);

  delay(205 * msPerTick);

  tone(8, 208);

  delay(239 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 311);

  delay(365 * msPerTick);

  noTone(8);

  delay(121 * msPerTick);

  tone(8, 277);

  delay(489 * msPerTick);

  noTone(8);

  delay(496 * msPerTick);

  tone(8, 208);

  delay(84 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 233);

  delay(96 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 277);

  delay(89 * msPerTick);

  noTone(8);

  delay(6 * msPerTick);

  tone(8, 233);

  delay(119 * msPerTick);

  noTone(8);

  delay(25 * msPerTick);

  tone(8, 349);

  delay(291 * msPerTick);

  noTone(8);

  delay(109 * msPerTick);

  tone(8, 349);

  delay(325 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 311);

  delay(516 * msPerTick);

  noTone(8);

  delay(205 * msPerTick);

  tone(8, 208);

  delay(89 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 233);

  delay(101 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 262);

  delay(120 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 233);

  delay(134 * msPerTick);

  noTone(8);

  delay(36 * msPerTick);

  tone(8, 415);

  delay(435 * msPerTick);

  noTone(8);

  delay(30 * msPerTick);

  tone(8, 262);

  delay(249 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 277);

  delay(226 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 262);

  delay(109 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 233);

  delay(240 * msPerTick);

  noTone(8);

  delay(106 * msPerTick);

  tone(8, 208);

  delay(99 * msPerTick);

  noTone(8);

  delay(6 * msPerTick);

  tone(8, 233);

  delay(124 * msPerTick);

  noTone(8);

  delay(15 * msPerTick);

  tone(8, 262);

  delay(121 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 233);

  delay(150 * msPerTick);

  noTone(8);

  delay(15 * msPerTick);

  tone(8, 277);

  delay(474 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 311);

  delay(220 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 262);

  delay(406 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 233);

  delay(124 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 208);

  delay(251 * msPerTick);

  noTone(8);

  delay(165 * msPerTick);

  tone(8, 208);

  delay(220 * msPerTick);

  noTone(8);

  delay(10 * msPerTick);

  tone(8, 311);

  delay(240 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 277);

  delay(209 * msPerTick);

  noTone(8);

  delay(40 * msPerTick);

  tone(8, 277);

  delay(560 * msPerTick);

  noTone(8);

  delay(1115 * msPerTick);

  tone(8, 277);

  delay(141 * msPerTick);

  noTone(8);

  delay(115 * msPerTick);

  tone(8, 277);

  delay(225 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 233);

  delay(230 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 277);

  delay(274 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 311);

  delay(391 * msPerTick);

  noTone(8);

  delay(524 * msPerTick);

  tone(8, 262);

  delay(236 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 233);

  delay(245 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 262);

  delay(84 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 233);

  delay(155 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 208);

  delay(861 * msPerTick);

  noTone(8);

  delay(329 * msPerTick);

  tone(8, 233);

  delay(145 * msPerTick);

  noTone(8);

  delay(91 * msPerTick);

  tone(8, 233);

  delay(235 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 262);

  delay(234 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 277);

  delay(235 * msPerTick);

  noTone(8);

  delay(11 * msPerTick);

  tone(8, 233);

  delay(230 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 208);

  delay(335 * msPerTick);

  noTone(8);

  delay(405 * msPerTick);

  tone(8, 415);

  delay(170 * msPerTick);

  noTone(8);

  delay(65 * msPerTick);

  tone(8, 415);

  delay(249 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 311);

  delay(441 * msPerTick);

  noTone(8);

  delay(20 * msPerTick);

  tone(8, 349);

  delay(230 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 311);

  delay(219 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 277);

  delay(326 * msPerTick);

  noTone(8);

  delay(150 * msPerTick);

  tone(8, 277);

  delay(139 * msPerTick);

  noTone(8);

  delay(96 * msPerTick);

  tone(8, 277);

  delay(239 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 233);

  delay(235 * msPerTick);

  noTone(8);

  delay(10 * msPerTick);

  tone(8, 277);

  delay(236 * msPerTick);

  noTone(8);

  delay(15 * msPerTick);

  tone(8, 233);

  delay(220 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 277);

  delay(259 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 311);

  delay(236 * msPerTick);

  noTone(8);

  delay(245 * msPerTick);

  tone(8, 262);

  delay(234 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 233);

  delay(245 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 262);

  delay(106 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 233);

  delay(140 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 208);

  delay(560 * msPerTick);

  noTone(8);

  delay(620 * msPerTick);

  tone(8, 233);

  delay(125 * msPerTick);

  noTone(8);

  delay(114 * msPerTick);

  tone(8, 233);

  delay(230 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 262);

  delay(241 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 277);

  delay(225 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 233);

  delay(229 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 208);

  delay(471 * msPerTick);

  noTone(8);

  delay(274 * msPerTick);

  tone(8, 311);

  delay(245 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 349);

  delay(185 * msPerTick);

  noTone(8);

  delay(71 * msPerTick);

  tone(8, 349);

  delay(439 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 311);

  delay(550 * msPerTick);

  noTone(8);

  delay(186 * msPerTick);

  tone(8, 277);

  delay(1154 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 311);

  delay(250 * msPerTick);

  noTone(8);

  delay(6 * msPerTick);

  tone(8, 349);

  delay(230 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 311);

  delay(415 * msPerTick);

  noTone(8);

  delay(49 * msPerTick);

  tone(8, 311);

  delay(180 * msPerTick);

  noTone(8);

  delay(71 * msPerTick);

  tone(8, 311);

  delay(250 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 349);

  delay(215 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 311);

  delay(229 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 208);

  delay(195 * msPerTick);

  noTone(8);

  delay(71 * msPerTick);

  tone(8, 208);

  delay(745 * msPerTick);

  noTone(8);

  delay(445 * msPerTick);

  tone(8, 208);

  delay(210 * msPerTick);

  noTone(8);

  delay(34 * msPerTick);

  tone(8, 233);

  delay(215 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 262);

  delay(261 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 277);

  delay(225 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 233);

  delay(249 * msPerTick);

  noTone(8);

  delay(246 * msPerTick);

  tone(8, 311);

  delay(245 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 349);

  delay(219 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 311);

  delay(415 * msPerTick);

  noTone(8);

  delay(301 * msPerTick);

  tone(8, 208);

  delay(114 * msPerTick);

  noTone(8);

  delay(6 * msPerTick);

  tone(8, 233);

  delay(100 * msPerTick);

  noTone(8);

  delay(4 * msPerTick);

  tone(8, 277);

  delay(96 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 233);

  delay(114 * msPerTick);

  noTone(8);

  delay(20 * msPerTick);

  tone(8, 349);

  delay(261 * msPerTick);

  noTone(8);

  delay(144 * msPerTick);

  tone(8, 349);

  delay(330 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 311);

  delay(496 * msPerTick);

  noTone(8);

  delay(225 * msPerTick);

  tone(8, 208);

  delay(84 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 233);

  delay(110 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 262);

  delay(116 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 233);

  delay(119 * msPerTick);

  noTone(8);

  delay(25 * msPerTick);

  tone(8, 311);

  delay(315 * msPerTick);

  noTone(8);

  delay(96 * msPerTick);

  tone(8, 311);

  delay(329 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 277);

  delay(281 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 262);

  delay(115 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 233);

  delay(265 * msPerTick);

  noTone(8);

  delay(54 * msPerTick);

  tone(8, 208);

  delay(111 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 233);

  delay(130 * msPerTick);

  noTone(8);

  delay(10 * msPerTick);

  tone(8, 262);

  delay(94 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 233);

  delay(106 * msPerTick);

  noTone(8);

  delay(15 * msPerTick);

  tone(8, 277);

  delay(435 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 311);

  delay(249 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 262);

  delay(226 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 233);

  delay(235 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 208);

  delay(274 * msPerTick);

  noTone(8);

  delay(205 * msPerTick);

  tone(8, 208);

  delay(241 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 311);

  delay(365 * msPerTick);

  noTone(8);

  delay(119 * msPerTick);

  tone(8, 277);

  delay(491 * msPerTick);

  noTone(8);

  delay(494 * msPerTick);

  tone(8, 208);

  delay(86 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 233);

  delay(94 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 277);

  delay(91 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 233);

  delay(120 * msPerTick);

  noTone(8);

  delay(25 * msPerTick);

  tone(8, 349);

  delay(289 * msPerTick);

  noTone(8);

  delay(111 * msPerTick);

  tone(8, 349);

  delay(325 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 311);

  delay(514 * msPerTick);

  noTone(8);

  delay(205 * msPerTick);

  tone(8, 208);

  delay(91 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 233);

  delay(99 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 262);

  delay(121 * msPerTick);

  noTone(8);

  delay(4 * msPerTick);

  tone(8, 233);

  delay(136 * msPerTick);

  noTone(8);

  delay(35 * msPerTick);

  tone(8, 415);

  delay(434 * msPerTick);

  noTone(8);

  delay(30 * msPerTick);

  tone(8, 262);

  delay(251 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 277);

  delay(225 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 262);

  delay(110 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 233);

  delay(240 * msPerTick);

  noTone(8);

  delay(104 * msPerTick);

  tone(8, 208);

  delay(101 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 233);

  delay(125 * msPerTick);

  noTone(8);

  delay(15 * msPerTick);

  tone(8, 262);

  delay(119 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 233);

  delay(150 * msPerTick);

  noTone(8);

  delay(15 * msPerTick);

  tone(8, 277);

  delay(476 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 311);

  delay(220 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 262);

  delay(404 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 233);

  delay(126 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 208);

  delay(249 * msPerTick);

  noTone(8);

  delay(166 * msPerTick);

  tone(8, 208);

  delay(219 * msPerTick);

  noTone(8);

  delay(10 * msPerTick);

  tone(8, 311);

  delay(240 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 277);

  delay(211 * msPerTick);

  noTone(8);

  delay(40 * msPerTick);

  tone(8, 277);

  delay(560 * msPerTick);

  noTone(8);

  delay(400 * msPerTick);

  tone(8, 208);

  delay(114 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 233);

  delay(101 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 277);

  delay(95 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 233);

  delay(114 * msPerTick);

  noTone(8);

  delay(20 * msPerTick);

  tone(8, 349);

  delay(261 * msPerTick);

  noTone(8);

  delay(144 * msPerTick);

  tone(8, 349);

  delay(330 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 311);

  delay(496 * msPerTick);

  noTone(8);

  delay(225 * msPerTick);

  tone(8, 208);

  delay(84 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 233);

  delay(111 * msPerTick);

  noTone(8);

  delay(4 * msPerTick);

  tone(8, 262);

  delay(116 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 233);

  delay(119 * msPerTick);

  noTone(8);

  delay(25 * msPerTick);

  tone(8, 311);

  delay(315 * msPerTick);

  noTone(8);

  delay(96 * msPerTick);

  tone(8, 311);

  delay(329 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 277);

  delay(281 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 262);

  delay(114 * msPerTick);

  noTone(8);

  delay(6 * msPerTick);

  tone(8, 233);

  delay(265 * msPerTick);

  noTone(8);

  delay(54 * msPerTick);

  tone(8, 208);

  delay(111 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 233);

  delay(129 * msPerTick);

  noTone(8);

  delay(11 * msPerTick);

  tone(8, 262);

  delay(95 * msPerTick);

  noTone(8);

  delay(4 * msPerTick);

  tone(8, 233);

  delay(106 * msPerTick);

  noTone(8);

  delay(15 * msPerTick);

  tone(8, 277);

  delay(435 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 311);

  delay(249 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 262);

  delay(226 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 233);

  delay(235 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 208);

  delay(274 * msPerTick);

  noTone(8);

  delay(205 * msPerTick);

  tone(8, 208);

  delay(241 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 311);

  delay(365 * msPerTick);

  noTone(8);

  delay(119 * msPerTick);

  tone(8, 277);

  delay(491 * msPerTick);

  noTone(8);

  delay(494 * msPerTick);

  tone(8, 208);

  delay(86 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 233);

  delay(94 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 277);

  delay(91 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 233);

  delay(120 * msPerTick);

  noTone(8);

  delay(25 * msPerTick);

  tone(8, 349);

  delay(289 * msPerTick);

  noTone(8);

  delay(111 * msPerTick);

  tone(8, 349);

  delay(325 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 311);

  delay(514 * msPerTick);

  noTone(8);

  delay(205 * msPerTick);

  tone(8, 208);

  delay(91 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 233);

  delay(99 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 262);

  delay(121 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 233);

  delay(135 * msPerTick);

  noTone(8);

  delay(35 * msPerTick);

  tone(8, 415);

  delay(434 * msPerTick);

  noTone(8);

  delay(30 * msPerTick);

  tone(8, 262);

  delay(251 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 277);

  delay(225 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 262);

  delay(110 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 233);

  delay(240 * msPerTick);

  noTone(8);

  delay(104 * msPerTick);

  tone(8, 208);

  delay(101 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 233);

  delay(125 * msPerTick);

  noTone(8);

  delay(15 * msPerTick);

  tone(8, 262);

  delay(119 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 233);

  delay(150 * msPerTick);

  noTone(8);

  delay(15 * msPerTick);

  tone(8, 277);

  delay(476 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 311);

  delay(220 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 262);

  delay(405 * msPerTick);

  noTone(8);

  delay(4 * msPerTick);

  tone(8, 233);

  delay(126 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 208);

  delay(249 * msPerTick);

  noTone(8);

  delay(166 * msPerTick);

  tone(8, 208);

  delay(219 * msPerTick);

  noTone(8);

  delay(10 * msPerTick);

  tone(8, 311);

  delay(240 * msPerTick);

  noTone(8);

  delay(5 * msPerTick);

  tone(8, 277);

  delay(211 * msPerTick);

  noTone(8);

  delay(40 * msPerTick);

  tone(8, 277);

  delay(560 * msPerTick);

  noTone(8);
}
